# Copie archiviate delle liste DDA AGCOM

L'Autorità per le Garanzie nelle Comunicazioni **AGCOM** vigila sul rispetto delle disposizioni del "Regolamento in materia di tutela del diritto d'autore sulle reti di comunicazione elettronica e procedure attuative ai sensi del decreto legislativo 9 aprile 2003, n. 70" - adottato con la delibera n. [680/13/CONS](https://www.agcom.it/visualizza-documento/2fb37939-620c-410d-a23f-2150d505b103) del 12 dicembre 2013, successivamente modificato dalla delibera n. [490/18/CONS](https://www.agcom.it/visualizza-documento/875626ba-5956-400f-baf4-86fd6ffbe073).

Nell'ambito delle sue competenze ed attività a tutela del diritto d'autore pubblica sia le statistiche (aggiornate periodicamente) sulle istanze presentate dai titolari che i dettagli dei relativi provvedimenti associati.

Tali pubblicazioni sono disponibili e consultabili liberamente presso la pagina dedicata https://www.agcom.it/interventi-dell-autorita-a-tutela-del-diritto-d-autore .

\
Contestualmente all'adozione di ciascun nuovo provvedimento inibitorio ([_cfr. relativo elenco_](https://www.agcom.it/provvedimenti-a-tutela-del-diritto-d-autore)) viene aggiornato l'elenco di siti inibiti - per i quali è stato comunicato ordine di blocco agli operatori.

Elenco disponibile anch'esso per la consultazione pubblica, essendo inserito tra gli allegati dei provvedimenti.

\
Dato che tale elenco viene aggiornato a seguito di ogni nuovo provvedimento con ordine di disabilitazione/blocco, questa cartella contiene copie archiviate degli elenchi utilizzati per le analisi, onde agevolare verifiche/validazioni terze dei risultati tramite urlscan e/o altri strumenti/metodi.

| Versione Elenco    | Data Pubblicazione | Link originale |
|--------------------|--------------------|----------------|
| 0000256_2023.22.03 | 24 Marzo 2023      | [Allegato B a Delibera n. 54/23/CSP](https://www.agcom.it/documents/10179/29792795/Allegato+22-3-2023+1679503680671/7c26157e-0c49-4efe-b228-00e2d9bce67a?version=1.0) |