# Report DDA 20230326

- **submission** = report di inoltro alle API urlscan per l'avvio del processo di analisi. Indicano anche quali siti non siano stati processati causa dominio scaduto / NXDOMAIN.

- **processed** = report sintetici degli URL processati OK. Disponibili nei formati CSV - JSON - PDF (_con gli URL per il recupero diretto via API/Web dei report dettagliati delle singole scansioni_).