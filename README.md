# DDA Notes

**_Cronologia :_**
- **_28 Marzo 2023_** - _addendum = filtraggio IP caso Telerium_
- **_26 Marzo 2023_** - _rilascio report + annotazioni_

\
L'Autorità per le Garanzie nelle Comunicazioni **AGCOM** vigila sul rispetto delle disposizioni del "Regolamento in materia di tutela del diritto d'autore sulle reti di comunicazione elettronica e procedure attuative ai sensi del decreto legislativo 9 aprile 2003, n. 70" - adottato con la delibera n. [680/13/CONS](https://www.agcom.it/visualizza-documento/2fb37939-620c-410d-a23f-2150d505b103) del 12 dicembre 2013, successivamente modificato dalla delibera n. [490/18/CONS](https://www.agcom.it/visualizza-documento/875626ba-5956-400f-baf4-86fd6ffbe073).

Nell'ambito delle sue competenze ed attività a tutela del diritto d'autore pubblica in una [pagina dedicata sul suo sito](https://www.agcom.it/interventi-dell-autorita-a-tutela-del-diritto-d-autore) sia le statistiche (_aggiornate mensilmente_) sulle istanze presentate dai titolari che i dettagli dei relativi provvedimenti associati.

\
Contestualmente all'adozione di ciascun nuovo provvedimento inibitorio ([_cfr. relativo elenco_](https://www.agcom.it/provvedimenti-a-tutela-del-diritto-d-autore)) viene aggiornato l'elenco di siti inibiti - per i quali è stato comunicato ordine di blocco agli operatori.

Elenco disponibile anch'esso per la consultazione pubblica, essendo inserito tra gli allegati dei provvedimenti.

Questa repository contiene i riferimenti di analisi effettuate in data 26/03/2023 tramite il servizio [urlscan.io](https://urlscan.io/about/) sugli URL in tale elenco - **versione 0000256_2023.22.03**.

-------------

### Indice

- 1. [**Metodologia usata**](#1-metodologia-usata)

- - 1.1 [Controllo formale URL](#11-controllo-formale-url)

- - 1.2 [Condizioni di scansione](#12-condizioni-di-scansione)

- - 1.3 [Report delle scansioni](#13-report-delle-scansioni)

---------------

- 2. [**Rete**](#2-rete)

- - 2.1 [AS ed IP](#21-as-ed-ip)

- - 2.2 [IP condivisi](#22-ip-condivisi)

---------------

- 3. [**Considerazioni**](#3-considerazioni)

---------------

- 4. [**Appendici**](#4-appendici)

- - 4.1 [Filtraggio IP caso Telerium](#41-filtraggio-ip-caso-telerium)

---------------

## 1. Metodologia usata

### 1.1 Controllo formale URL

_**Nota :** Nella cartella [Archivio_liste_DDA](Archivio_liste_DDA) sono archiviate le copie delle versioni degli elenchi utilizzati._

| versione elenco DDA  | URL inibiti  | URL univoci in elenco (*) |
|----------------------|--------------|---------------------------|
| 0000256_2023.22.03   | 3221         | 2341                      |

> _(*) a seguito di rimozione URL duplicati_

----------------
----------------

### 1.2 Condizioni di scansione

Le scansioni sono state gestite tramite 5 API worker free-tier - attivi in parallelo & tarati con i seguenti vincoli per l'inoltro degli URL :

- delay base di 8s per url-submission singolo worker
- delay base di 300s da url-submission OK per recupero risultato

| URL               | Worker-1 | Worker-2 | Worker-3 | Worker-4 | Worker-5 | **TOTALI** |
|-------------------|----------|----------|----------|----------|----------|------------|
| Inoltrati         | 645      | 645      | 645      | 641      | 641      | **3221**   |
| Accettati **(!)** | 425      | 446      | 484      | 491      | 495      | **2341**  |

> _**(!)** Respinti 880 URL causa KO risoluzione DNS = dominio scaduto / NXDOMAIN_

\
**Gli elenchi degli URL inoltrati sono disponibili nella cartella [Elenchi_Siti](Elenchi_Siti)**.

![alt text](Images/12_elenchi_siti.png "Elenchi degli URL inoltrati alle API urlscan")

-----------------
-----------------

### 1.3 Report delle scansioni

Il processo degli URL accettati è stato effettuato con le seguenti opzioni di configurazione worker :

- `visibility:public` = risultati pubblici & consultabili tramite urlscan senza vincolo di account

- `scanner.country:it` = worker client con indirizzo IP italiano di uscita

- `useragent:default` = User Agent impostato come ultima versione stabile di Google Chrome per Windows 10

- `upgrade-insecure-requests:1` = verifica upgrade HTTP->HTTPS

| URL Accettati                           | **2341** |
|-----------------------------------------|----------|
| Scartati in pre-processo **(1)**        | _16_     |
| KO connessione durante processo **(2)** | _77_     |
|                                         |          |
| **Processati OK**                       | **2248** |

> _**(1)** incompatibili con l'impostazione `visibility:public` per i risultati delle relative scansioni_
>
> _**(2)** urlscan ha restituito un risultato di Connection Failure - KO raggiungibilità indirizzo IP associato. Situazione persistente, anche variando IPv4/IPv6 (ove presente risoluzione dual-stack) e protocolli HTTP/HTTPS per l'invio delle request_

\
**I rapporti di inoltro (prefisso _submission_) e di processo (prefisso _processed_) delle scansioni sono disponibili nella cartella [Report](Report)**.

I rapporti sono disponibili in più formati & contengono gli URL diretti per la consultazione web (`result`) e/o il recupero JSON (`api`) dei risultati dettagliati delle singole scansioni.


\
In virtù dell'impostazione `visibility:public` i risultati dettagliati delle singole scansioni sono disponibili pubblicamente su urlscan & accessibili da chiunque, anche senza necessità di account/autenticazione.

Possono essere quindi recuperati tramite :

1. [Ricerca web UI](https://urlscan.io/search/) / [Search API](https://urlscan.io/docs/api/#search), impostando le query ([sintassi ElasticSearch](https://urlscan.io/docs/search/)) con i riferimenti dei `task.tags` associati

2. [Result API](https://urlscan.io/docs/api/#result), indicando direttamente i riferimenti identificativi (UUID) dei risultati di scansione 

\
Si riportano i `task.tags` utilizzati per contrassegnare le attività, unitamente ai link delle ricerche web con i risultati.

| TAG elenco DDA       | Link diretto su urlscan |
|----------------------|-------------------------|
| `AGCOM-DDA-20230323` | [Risultati processo](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)) |

------------------
------------------
------------------

## 2. Rete

### 2.1 AS ed IP

Gli indirizzi IP IPv4/IPv6 dei 2325 URL univoci processati risultano associati ai sistemi autonomi (AS) di 121 provider/LIR.

6 provider/LIR ospitano ciascuno almeno 50 siti. 

| Filtro query AS              | Provider/LIR     | Match            |
|------------------------------|------------------|------------------|
| `page.asnname:cloudflare*`   | Cloudflare       | [833](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Acloudflare*))   |
| `page.asnname:amazon*`       | Amazon           | [607](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Aamazon*))       |
| `page.asnname:teaminternet*` | Team Internet AG | [133](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Ateaminternet*)) |
| `page.asnname:akamai*`       | Akamai           | [132](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Aakamai*))       |
| `page.asnname:sedo*`         | Sedo **(!)**     | [122](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Asedo*))         |
| `page.asnname:google*`       | Google           | [78](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.asnname%3Agoogle*))       |

> _**(!)** siti associati a domini scaduti, che sono stati (ri)presi da Sedo per la vendita sulla sua piattaforma_

Risultandovi associati 1905 siti complessivamente = **81.9% del totale processato**.

------------

\
**798 siti (34.3% del totale processato) supportano pienamente connessione & accesso su IPv6**.

Gli altri siti o sono raggiungibili solo su IPv4 oppure accettano anche la connessione, ma poi switchano comunque la navigazione su IPv4. 

| Filtro query IPv6 sito | Descrizione                   | Match          |
|------------------------|-------------------------------|----------------|
| `page.ip:2000\:\:\/3`  | Check DNS/connessione su IPv6 | [798](https://urlscan.io/search/#(task.tags%3AAGCOM-DDA-20230323)%20AND%20(page.ip%3A2000%5C%3A%5C%3A%5C%2F3))  

----------------------
----------------------

### 2.2 IP condivisi

Solo osservando la situazione/distribuzione AS per quanto concerne protezione siti e shared-hosting, si intuisce che **per la netta maggioranza (_>80%_) dei siti attualmente inibiti in elenco DDA NON possa essere soddisfatta a priori una condizione di corrispondenza biunivoca tra sito/dominio e rispettive risoluzioni IPv4/IPv6**.

\
Analizzando ulteriormente i risultati delle scansioni 26/03 urlscan, si individuano 581 risoluzioni IPv4/IPv6 uniche - limitatamente al set di siti inibiti = più siti in elenco DDA risultavano nelle scansioni con risoluzioni sui medesimi indirizzi IPv4/IPv6.

Sebbene sia complesso verificare in modo certo che un IP sia effettivamente dedicato / NON condiviso, è comunque possibile procedere - anche velocemente - con alcuni controlli di esclusione.

\
Effettuando già un primo/veloce controllo incrociato IP/hostname, e.g. tramite le [reverse API ViewDNS](https://viewdns.info/api/docs/reverse-ip-lookup.php), risulta il seguente quadro d'incrocio con i record ViewDNS per il mese di Marzo 2023.

| IPv4/IPv6 incrociati con record ViewDNS 03/2023 | 581     |
|-------------------------------------------------|---------|
| IP non presenti in record ViewDNS 03/2023       | 14      |
| IP in record 03/2023 con `domain_count = 1`     | 56      |

Pertanto, **per 511 risoluzioni IPv4/IPv6 vi sarebbe improcedibilità _instant_ per aggiunta filtraggio IP** in caso di ipotetica live-injunction odierna/ravvicinata (_ordine di blocco entro 30 min. da istanza_) = condivisione con siti NON in elenco DDA - accertata in arco temporale ristretto (<30gg).

\
Passando i 70 indirizzi IP residui ad un veloce controllo supplementare, e.g. tramite le [reverse API TIP](https://threatintelligenceplatform.com/reverse-ip-lookup-api), **le risoluzioni IPv4/IPV6 con improcedibilità per filtraggio IP diventerebbero 578** = non risulterebbero infatti record recenti/realtime di condivisione extra-elenco DDA soltanto per i seguenti **3 indirizzi IPv4**.

| Indirizzo IP   | URL associato      | GeoIP/Country |
|----------------|--------------------|---------------|
| 139.59.58.210  | `api.deyga.in`     | India         |
| 176.123.2.117  | `torlock.com`      | Moldova       |
| 178.218.209.92 | `www.goldenmp3.ru` | Russia        |

----------------------
----------------------
----------------------

## 3. Considerazioni

**Giorgio Bonfiglio** ha riassunto esaustivamente in un [suo thread su Twitter](https://twitter.com/g_bonfiglio/thread/1639577979272548354) le varie criticità di efficacia & efficienza dell'eventuale aggiunta del filtraggio IP agli ordini di blocco DDA.

Criticità riscontrate anche nell'analisi dell'attuale elenco DDA.

Effettuando infatti controlli veloci & compatibili con una finestra temporale di 30 minuti per applicazione blocco live-injunction, **solo per lo 0.1% dei siti processati sarebbe eventualmente valutabile un filtraggio IP - sempre/comunque con rischio di over-blocking**.

\
Rischio mitigabile con impostazione ravvicinata di scadenza/lift blocco IP (_e.g. in best-effort 24h_) & vincoli di applicazione (_e.g. solo operatori fascia A con >100k linee dati attive & impostazione blocco solo per utenze residential/consumer_), ma sempre/comunque non azzerabile. 

Pertanto, sebbene [l'art.2 della recente proposta di legge C.648](https://www.camera.it/leg19/126?tab=2&leg=19&idDocumento=648&sede=&tipo=) contempli la possibilità per i titolari di richiedere direttamente anche il filtraggio IP nelle [istanze DDA](https://ddaonline.agcom.it/) di live-blocking, permangono condizioni ostative di natura tecnica & operativa per una sua effettiva implementazione nella maggioranza dei casi reali.

\
Infatti, come richiamato anche da AIIP nel suo [intervento del 3 Febbraio 2023 presso la Camera dei Deputati](https://www.camera.it/application/xmanager/projects/leg19/attachments/upload_file_doc_acquisiti/pdfs/000/008/149/Memoria_AIIP.pdf), l'implementazione di procedure abbreviate/automatizzate _"inaudita altera parte"_ per live-injunction NON dovrebbe comunque prescindere da meccanismi di protezione - _non solo come tutela giuridica a posteriori per gli operatori/esecutori dei blocchi, ma anche come misure tecniche (e.g. controlli shared hostname/ip e whitelist), onde prevenire/minimizzare il processo di segnalazioni errate/imprecise/sproporzionate_.

Osservazioni, che peraltro sono state presumibilmente già esposte anche in ambito di consultazione tra AGCOM e titolari/operatori/interessati - cfr. [Delibera n. 445/22/CONS AGCOM](https://www.agcom.it/documentazione/documento?p_p_auth=fLw7zRht&p_p_id=101_INSTANCE_FnOw5lVOIXoE&p_p_lifecycle=0&p_p_col_id=column-1&p_p_col_count=1&_101_INSTANCE_FnOw5lVOIXoE_struts_action=/asset_publisher/view_content&_101_INSTANCE_FnOw5lVOIXoE_assetEntryId=29133099&_101_INSTANCE_FnOw5lVOIXoE_type=document).

Proprio perché _sh*t happens_ e già con le procedure non abbreviate (_cioè con contradditorio/istruttoria_) possono pure capitare over-blocking per filtraggio DNS a seguito di ordini delle Autorità competenti per vigilanza - cfr. [Medium con ADM a Marzo 2021](https://twitter.com/relationsatwork/status/1367470887172268036).

----------------------
----------------------
----------------------

## 4. Appendici

### 4.1 Filtraggio IP caso Telerium

Nel precedente [paragrafo 2.2](#22-ip-condivisi) ho mostrato come/perché la misura del filtraggio IP sarebbe applicabile con basso rischio over-blocking soltanto a 3 URL (_0.1% del totale_) presenti nell'elenco DDA - versione 0000256_2023.22.03.

Tuttavia, tanto desideri/interessi dei titolari quanto [l'art.2 della recente proposta di legge C.648](https://www.camera.it/leg19/126?tab=2&leg=19&idDocumento=648&sede=&tipo=) non mirano a tali siti come target primari di istanze DDA live-blocking con filtraggio IP, bensì alle sorgenti dei flussi streaming audio/video. 

\
Proviamo quindi a simulare un caso di live-injunction con istanza DDA in cui il titolare richieda/indichi inibizione indirizzo IP, riutilizzando i dati di un provvedimento recente AGCOM.

Ad esempio quelli della [Delibera n. 54/23/CSP](https://www.agcom.it/visualizza-documento/b449573b-4f6d-40b5-9601-c71c7b6a4b30) relativa all'ordine di blocco tramite filtraggio DNS del sito `telerium.me` - emesso dopo OK istruttoria su rispettiva istanza DDA presentata da DAZN.

\
Ipotizziamo che DAZN voglia procedere con istanza DDA congiunta di filtraggio DNS+IP - indicando direttamente nel modulo più riferimenti URL/IP correlati da bloccare - per la pagina su `https://telerium.de/canal-31.php`.

Dovrà quindi procedere con visita/ispezione della pagina, in modo da analizzare le transazioni & individuare domini/IP da segnalare.

\
Consideriamo che ottenga il seguente output https://urlscan.io/result/f122d02c-ec6b-487d-aeed-e7278829c9a4/ .

\
Dovrà ovviamente scartare domini/IP relativi a servizi legit (e.g. CDN, statistics e ads), limitandosi solo a domini/IP univocamente destinati/usati per attività/servizi in violazione.

| Dominio              | AS/IP                                                        | Descrizione |
|----------------------|--------------------------------------------------------------|-------------|
| `telerium.de`        | [Cloudflare](https://urlscan.io/domain/telerium.de)          | Frontend    |
| `casadelfutbol.tv`   | [Cloudflare](https://urlscan.io/domain/casadelfutbol.tv)     | Player      |
| `casadelfutbol.info` | [Cloudflare](https://urlscan.io/domain/casadelfutbol.info)   | Player      |
| `godzillive.com`     | [Cloudflare](https://urlscan.io/domain/godzillive.com)       | Feed        |
| `s5.flowerscast.com` | [37.49.224.65](https://urlscan.io/domain/s5.flowerscast.com) | Stream      |

\
Farà quindi le verifiche sull'indirizzo IP 37.49.224.65, in modo da appurare se sia condiviso (o meno) con servizi legit & determinare il grado di rischio over-blocking.

Ipotizziamo che proceda velocemente tramite servizi API (_ViewDNS, TIP, RiskIQ, etc._), ottenendo il seguente quadro di record recenti/realtime per le risoluzioni associate a tale indirizzo IP.

| Risoluzioni su 37.49.224.65 | Primo Record | **Ultimo Record** |
|-----------------------------|--------------|-------------------|
| `s5.flowerscast.com`        | 2023-01-16   | **2023-03-28**    |		
| `t5.switchcast2.com`        | 2022-11-18   | **2023-03-28**    |	
| `appserv9.xyz`              | 2022-05-04   | **2023-03-28**    |

\
Data l'assenza di evidenze immediate di condivisione IP con servizi legit/well-known, potrà già definire i seguenti campi nel modulo d'istanza DDA live-blocking :

| Risorsa              | Blocco richiesto |
|----------------------|------------------|
| `telerium.de`        | Filtraggio DNS   |
| `casadelfutbol.tv`   | Filtraggio DNS   |
| `casadelfutbol.info` | Filtraggio DNS   |
| `godzillive.com`     | Filtraggio DNS   |
| `s5.flowerscast.com` | Filtraggio DNS   |
| `37.49.224.65`       | Filtraggio IP    |

----------------

\
Tuttavia, un modulo con tali campi compilati potrebbe non essere ancora efficace in ottica live-blocking.

I siti di stream illeciti non usano infatti sempre un'unica sorgente stream/cast statica per la trasmissione di un evento/canale. Possono anche selezionare dinamicamente tra più sorgenti tramite esecuzione di appositi script JS.

Ciò accade anche in questo caso = eseguiti script ospitati su `cdn.jsdelivr.net` con richiamo feed URL da `godzillive.com` (_entrambi NON bloccabili tramite filtraggio IP_), onde selezionare la sorgente in funzione di evento e parametri client.

\
Consideriamo che, ripetendo dopo pochi minuti visita/ispezione della pagina, si ottenga questo nuovo output https://urlscan.io/result/9fda12ed-5df2-4f74-8623-6f13f3858655/ .

\
Analizzando tale output, risulterà la variazione del dominio sorgente richiamato per stream/cast dell'evento.

| Dominio               | AS/IP                                                           | Descrizione |
|-----------------------|-----------------------------------------------------------------|-------------|
| `s16.flowerscast.com` | [103.145.12.214](https://urlscan.io/domain/s16.flowerscast.com) | Stream      |

![alt text](Images/41_s16flowerscast.png "Richiamo della sorgente stream")

| Risoluzioni su 103.145.12.214 | Primo Record | **Ultimo Record** |
|-------------------------------|--------------|-------------------|
| `s16.flowerscast.com`         | 2023-01-28   | **2023-03-28**    |

\
Pertanto, il modulo dell'istanza DDA andrebbe già integrato/aggiornato in questo modo.

| Risorsa                   | Blocco richiesto     |
|---------------------------|----------------------|
| `telerium.de`             | Filtraggio DNS       |
| `casadelfutbol.tv`        | Filtraggio DNS       |
| `casadelfutbol.info`      | Filtraggio DNS       |
| `godzillive.com`          | Filtraggio DNS       |
| `s5.flowerscast.com`      | Filtraggio DNS       |
| `37.49.224.65`            | Filtraggio IP        |
| **`s16.flowerscast.com`** | **Filtraggio DNS**   |
| **`37.49.224.65`**        | **Filtraggio IP**    |


\
Ruotando visita/ispezione, si individuano così i seguenti riferimenti di sorgente stream per l'evento target.

| Dominio             | IP                                                              | Condivisioni IP    |
|---------------------|-----------------------------------------------------------------|--------------------|
|  s1.flowerscast.com | [37.49.224.186](https://urlscan.io/domain/s1.flowerscast.com)   | t1.switchcast2.com |
|  s4.flowerscast.com | [37.49.224.151](https://urlscan.io/domain/s4.flowerscast.com)   | t4.switchcast2.com |
|  s5.flowerscast.com | [37.49.224.65](https://urlscan.io/domain/s5.flowerscast.com)    | t5.switchcast2.com |
|  s6.flowerscast.com | [45.143.220.133](https://urlscan.io/domain/s6.flowerscast.com)  | s1.capoplay.com    |
|  s8.flowerscast.com | [37.49.224.185](https://urlscan.io/domain/s8.flowerscast.com)   | s3.capoplay.com    |
| s11.flowerscast.com | [45.143.220.139](https://urlscan.io/domain/s11.flowerscast.com) |                    |
| s12.flowerscast.com | [37.49.224.234](https://urlscan.io/domain/s12.flowerscast.com)  |                    | 
| s16.flowerscast.com | [103.145.12.214](https://urlscan.io/domain/s16.flowerscast.com) |                    | 

----------------------

\
Pertanto, il modulo risultante d'istanza DDA live-blocking prevederebbe i seguenti campi per controllo & applicazione filtraggio congiunto DNS+IP degli accessi ai contenuti illeciti della pagina su `https://telerium.de/canal-31.php` :

| Risorsa                   | Blocco richiesto     |
|---------------------------|----------------------|
| `telerium.de`             | Filtraggio DNS       |
|                           |                      |
| `casadelfutbol.tv`        | Filtraggio DNS       |
| `casadelfutbol.info`      | Filtraggio DNS       |
| `godzillive.com`          | Filtraggio DNS       |
|                           |                      |
| `s1.flowerscast.com`      | Filtraggio DNS       |
| `s4.flowerscast.com`      | Filtraggio DNS       |
| `s5.flowerscast.com`      | Filtraggio DNS       |
| `s6.flowerscast.com`      | Filtraggio DNS       |
| `s8.flowerscast.com`      | Filtraggio DNS       |
|`s11.flowerscast.com`      | Filtraggio DNS       |
|`s12.flowerscast.com`      | Filtraggio DNS       |
|`s16.flowerscast.com`      | Filtraggio DNS       |

| Risorsa                   | Blocco richiesto     |
|---------------------------|----------------------|
| `37.49.224.65`            | Filtraggio IP        |
| `37.49.224.151`           | Filtraggio IP        |
| `37.49.224.185`           | Filtraggio IP        |
| `37.49.224.186`           | Filtraggio IP        |
| `37.49.224.234`           | Filtraggio IP        |
|                           |                      |
| `45.143.220.133`          | Filtraggio IP        |
| `45.143.220.139`          | Filtraggio IP        |
|                           |                      |
| `103.145.12.214`          | Filtraggio IP        |


> _**Nota :** il filtraggio IP (indistintamente dalla porta remota) comporta comunque un rischio overblocking in caso e.g. di condivisioni IP con risorse legit, nonché o con eventuali stream leciti da medesimi IP, ma diverse porte remote._
>
> _Rischio solo mitigabile in tal caso con :_
> 
> - _**impostazione determinata di scadenza/lift blocco IP** entro termine evento/i o in best-effort 24h_
>
> _e/o_
>
> - _**vincoli di applicazione** e.g. solo operatori fascia A con >100k linee dati attive e/o impostazione blocco solo per utenze residential/consumer_
